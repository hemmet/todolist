# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create(name: 'Enes',email: 'met914@gmail.com',password: '123567890')
User.create(name: 'asd', email: 'asd@asd.com', password: 'qweqwe')
List.create(title: 'list_1', user_id: 2)
Item.create(name: 'item_1', done: false, due_date: DateTime.parse("2017-08-08 08:17:38"), list_id: 1)