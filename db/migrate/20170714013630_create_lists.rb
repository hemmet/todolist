class CreateLists < ActiveRecord::Migration[5.1]
  def change
    create_table :lists do |t|
      t.string :title, :limit => 50, :null => false, :default => ""
      t.references :user, foreign_key: true
      t.timestamps
    end
  end
end
