class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :name, :limit => 15, :null => false
      t.string :password, :limit => 15, :null => false
      t.string :email, :null => false
      t.timestamps
    end
    add_column :users, :fcm_token, :text
  end
end
