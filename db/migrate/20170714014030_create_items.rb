class CreateItems < ActiveRecord::Migration[5.1]
  def change
    create_table :items do |t|
      t.string :name, :limit => 50, :null => false, :default => ""
      t.boolean :done, :default => false
      t.references :list, foreign_key: true
      t.timestamps
      t.datetime :due_date
    end
  end
end
