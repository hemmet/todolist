class ItemController < ApplicationController
    
    #returns all items of the list
    def index
        @item = Item.where(list_id: params[:list_id])
        render json: @item
    end
    
    #if create operation is succesfull, returns the new item
    #if not, returns 'Failure'
    def create
        @item = Item.new(name: params[:item][:name], done: params[:item][:done], due_date: params[:item][:due_date], list_id: params[:list_id])
        if @item.save
            render json: [@item]
        else
            render json: [{:status=>'Failure'}]
        end
    end
    
    #finds the item and returns
    def show
        @item = Item.where(list_id: params[:list_id], id: params[:id])
        render json: @item
    end
    
    #if update operation is succesfull, returns the item with new info
    #if not, returns 'Failure'
    def update
        @item = Item.where(list_id: params[:list_id], id: params[:id])
        if @item.update_all(name: params[:item][:name], done: params[:item][:done], due_date: params[:item][:due_date])
            render json: @item
        else
            render json: [{:status=>'Failure'}]
        end
    end
    
    #if update operation is succesfull, returns 'Success'
    #if not, returns 'Failure'
    def destroy
        @item = Item.where(list_id: params[:list_id], id: params[:id]).first
        if @item.destroy
            render json: [{:status=>'Success'}]
        else
            render json: [{:status=>'Failure'}]
        end
    end

end