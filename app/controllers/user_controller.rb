class UserController < ApplicationController  
   
    #login
    def login 
        @user = User.where(user_params_for_login)
        
        if @user.length == 0
            render json: [{:status=>'Failure'}]
        else
            @user.each do |user|
                DuedateControlJob.set(wait: 10.seconds).perform_later user.id
            end
            render json: @user
        end
    end
    
    #if create operation is succesfull, returns the new user
    #if not, returns 'Failure'
    def create
        @user = User.new(user_params)
        if @user.save
            render json: [@user]
        else
            render json: [{:status=>'Failure'}]
        end
    end
    
    #finds the user and returns
    def show
        @user = User.where(id: params[:id])
        render json: @user
    end
    
    #if update operation is succesfull, returns the user with new info
    #if not, returns 'Failure'
    def update
        @user = User.where(id: params[:id])
        if @user.update_all(name: params[:user][:name], password: params[:user][:password], email: params[:user][:email])
            render json: @user
        else
            render json: [{:status=>'Failure'}]
        end
    end
    
    def notification
        now = 1.days.from_now
        arr = Array.new
        
        @lists = List.where(user_id: params[:user_id])
        @lists.each do |list|
            @items = Item.where(["done = ? and list_id = ? and due_date < ? ", false, list.id, now])
            @items.each do |item|
                arr.push(item.id)
            end
        end
        
        @items = Item.where({id: arr})
        render json: @items
    end
    
    def fcm_token
        user = User.where(id: params[:id]).first
        if user.update_attribute(:fcm_token, params[:fcm_token])
            render :json => { error: true }
        else
            render :json => { error: false }
        end
    end
    
  private
    def user_params
        params.require(:user).permit(:name, :email, :password)
    end
    
    def user_params_for_login
        params.require(:user).permit(:name, :password)
    end
    
end