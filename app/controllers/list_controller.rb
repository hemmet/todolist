class ListController < ApplicationController
    
    def index
        @list = List.where(user_id: params[:user_id])
        render json: @list
    end
    
    def create
        @list = List.new(title: params[:list][:title] , user_id: params[:user_id])
        if @list.save
            render json: [@list]
        else
            render json: [{:status=>'Failure'}]
        end
    end
    
    def show
        @list = List.where(id: params[:id], user_id: params[:user_id])
        render json: @list
    end
    
    def destroy
        @list = List.where(id: params[:id], user_id: params[:user_id]).first
        if @list.destroy
            render json: [{:status=>'Success'}]
        else
            render json: [{:status=>'Failure'}]
        end
    end
    
    def update
        @list = @list = List.where(id: params[:id], user_id: params[:user_id])
        if @list.update_all(title: params[:list][:title])
            render json: @list
        else
            render json: [{:status=>'Failure'}]
        end
    end
end