class DuedateControlJob < ApplicationJob
  require 'httparty'
  
  queue_as :default
  
  FIREBASE_SERVER_API_KEY = "AIzaSyBdj2akbMWaEjGn10m0BiWiQevE4kd0BKo"
  FIREBASE_NOTIFICATION_URL = "https://fcm.googleapis.com/fcm/send"
  
  def perform(id)
    user = User.where(id: id).first    
    lists = List.where(user_id: user.id)
    lists.each do |list|
      items = Item.where(list_id: list.id)
      items.each do |item|
      #if item.due_date < DateTime.now
        headers = {}
        headers.merge!({"Authorization" => "key=#{FIREBASE_SERVER_API_KEY}", "Content-Type" => "application/json"})
        params = {}
        params[:registration_ids] = Array.wrap(user.fcm_token)
        params[:priority] = "high"
        data = {}
        data[:notification_id] = user.id
        data[:title] = "Gecikme"
        data[:description] = {:due_date => item.due_date, :list => list.title, :item => item.name}
        params[:data] = data
        body = JSON.generate(params)
        HTTParty.post(FIREBASE_NOTIFICATION_URL,:body => body,:headers => headers)
      end
    end
  end
end
