class List < ApplicationRecord
    before_destroy :destroy_items
    
    has_many :items, dependent: :destroy
    belongs_to :user
    
    private
    
    def destroy_items
        self.items.delete_all
    end
end
