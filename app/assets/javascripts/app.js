'use strict';

var app = angular.module('myApp', ['templates',
  'ui.router',
  'ngRoute',
  'ngMaterial',
  'ngMdIcons'
]).config(['$routeProvider','$locationProvider', function($routeProvider, $locationProvider) {
   $locationProvider.html5Mode({
    enabled: true,
    requireBase: false
  });
  
  $routeProvider
  .when("/", {
    templateUrl: '_login.html',
    controller: 'loginController'
  })
  .when("/signUp" ,{
    templateUrl: "_signUp.html",
    controller: 'signUpController'
  })
  .when("/main/:userID", {
    templateUrl: "_main.html",
    controller: 'mainController'
  });
}])



.controller('appController', function($scope) {
  console.log("appController started");
})




.controller('loginController' , function($scope, $location, $http) {
  console.log("login controller started"); 
  $scope.hataMesajiGoster=false;
  var userID;
  $scope.username;
  $scope.password;
  
  $scope.logIn = function(){
    loginService($scope.username, $scope.password);
    
     
  };
  
 $scope.signUp = function(){
   console.log("sign up clicked!");
    $location.path("/signUp");
  }
  
  function loginService(name, password){
    var params = {
          "name": "",
          "password": ""
      };
      params.name = name;
      params.password = password;
      $http.post('/login', params).then(successCallback, errorCallback);
      function successCallback(response){
        $scope.hataMesajiGoster = false;
        console.log(response.data[0]);
        if(response.data.length == 0){
          $scope.hataMesajiGoster = true;
        }
        else{
          $scope.hataMesajiGoster = false;
          userID = response.data[0].id;
          $location.path("/main/" + userID); 
        }
      }
      function errorCallback(error){
        $scope.hataMesajiGoster = true;
        console.log(error);
      }
    }
  
})



.controller('signUpController', function($scope, $location, $http) {
  console.log("sginUp controller started");
  $scope.username;
  $scope.password;
  $scope.email;
    
    $scope.signUpNewUser = function(){
        console.log("sign up submit");
        signUpServices($scope.username, $scope.email, $scope.password);
    };   
    
    
    
    function signUpServices(name, email,password){
      var params = {
            "name": "",
            "password": "",
            "email:": ""
        };
        params.name = name;
        params.email = email;
        params.password = password;
      $http.post('/user/',params).then(successCallback, errorCallback);
      function successCallback(response){
        console.log(response.data);
        //failure control gelmeli
        var userID = response.data[0].id;
        console.log(userID);
        
        $location.path("/main/" + userID);
      }
      function errorCallback(error){
        console.log(error);
      }
    }
    
    

});


app.controller('mainController', function($scope, $location, $mdDialog, $http, $routeParams) {
  console.log("main controller started");
  
    var param1= $routeParams.userID;
    console.log(param1);
    userCredential();
    
    $scope.firstName = "";
    $scope.lists = [];
    $scope.gosterilenListId = -1;
    $scope.User;
    $scope.items =[];
    
    
  getAllLists();

   
    $scope.logOut = function(){
        $location.path("/"); 
        param1="";
    };
    
    $scope.secilenList = function(index){
      $scope.gosterilenListId = index;
      console.log("secilen index list: " + index);
      console.log("secilen list object");
      console.log($scope.lists[index]);
      $scope.gosterilenListId = $scope.lists[index].id;
      $scope.items=[];
      getSecilenListItem($scope.gosterilenListId);
    };
    
    $scope.editSelectedList = function(event, $index){
      $scope.editListName = $scope.lists[$index].title;
      $mdDialog.show({
                  clickOutsideToClose: true,
                  locals:{dataToPass: $scope.parentScopeData},     
                  scope: $scope,
                  parent: angular.element(document.body),
                  preserveScope: true,           
                  templateUrl:'_editList.html' ,
               });
      $scope.saveEditedList = function(){
        console.log($index);
        var index = $scope.lists[$index].id;
        editListService($scope.editListName, index);
        $mdDialog.hide();
      };
      $scope.abortEditList = function(){
        $mdDialog.hide();
      };
    };
    
    $scope.addNewList= function(event){
       $mdDialog.show({
                  clickOutsideToClose: true,
                  locals:{dataToPass: $scope.parentScopeData},     
                  scope: $scope,
                  parent: angular.element(document.body),
                  preserveScope: true,           
                  templateUrl:'_addNewList.html' ,
               });
      $scope.abortNewList = function(){
        $scope.newListName = "";
        $mdDialog.hide();
      };
      $scope.saveNewList = function(){
        console.log("new List added");
        createNewList($scope.newListName, $mdDialog);
        $scope.newListName = "";
      };
    };
    
    $scope.addNewItem = function(event){
      $scope.newItemName="";
      $scope.newItemDueDate ;
      $scope.newItemSaat;
       $mdDialog.show({
                  clickOutsideToClose: true,
                  locals:{dataToPass: $scope.parentScopeData},     
                  scope: $scope,
                  parent: angular.element(document.body),
                  preserveScope: true,           
                  templateUrl:'_addNewItem.html' ,
               });
      $scope.saveNewList = function(){
        console.log("new item added");
        
        
        var month;
          if($scope.newItemDueDate.getMonth()+1<10){
            var temp = $scope.newItemDueDate.getMonth()+1;
            month = '0' + temp;
          }
          else{
            month = month.getMonth()+1;
          }
          var date;
          if($scope.newItemDueDate.getDate()<10){
            date = '0' + $scope.newItemDueDate.getDate();
          }
          else{
            date = $scope.newItemDueDate.getDate();
          }
          
           var dueDate = $scope.newItemDueDate.getFullYear() + '-' +  month+ '-' + date;
          
          $scope.strtr = $scope.newItemSaat.toString();
          //console.log($scope.editItemSaat.getHours()+ " " + $scope.editItemSaat.getMinutes());
           var dueHour = $scope.strtr.substring(16,24);
           
           dueDate = dueDate + ' ' +dueHour;
        
        createNewItem($scope.newItemName, $scope.gosterilenListId, dueDate, $mdDialog);
        $scope.newItemName = "";
        $scope.newItemDueDate =null;
        $scope.newItemSaat = null;
        
      };
      $scope.abortNewItem = function(){
        $mdDialog.hide();
        $scope.newItemName = "";
      };
    };
    
    $scope.deleteList = function(event, $index){
      console.log($index);
      $scope.deleteListName = $scope.lists[$index].title;
       $mdDialog.show({
                  clickOutsideToClose: true,
                  locals:{dataToPass: $scope.parentScopeData},     
                  scope: $scope,
                  parent: angular.element(document.body),
                  preserveScope: true,           
                  templateUrl:'_deleteList.html' ,
               });
        $scope.deleteListCertain = function(){
          var index = $scope.lists[$index].id;
          deleteListService(index, $mdDialog);
        };
        $scope.abortDeleteList = function(){
          $mdDialog.hide();
        };
      console.log("list deleted action");
    };
    
    $scope.deleteItem = function(event, $index){
      $scope.deleteItemName = $scope.items[$index].name;
       $mdDialog.show({
                  clickOutsideToClose: true,
                  locals:{dataToPass: $scope.parentScopeData},     
                  scope: $scope,
                  parent: angular.element(document.body),
                  preserveScope: true,           
                  templateUrl:'_deleteItem.html' ,
               });
               $scope.deleteItemCertain = function(){
                 var itemId = $scope.items[$index].id;
                 deleteItemService(itemId,$mdDialog);
               };
               $scope.abortDeleteItem = function(){
                 $mdDialog.hide();
               };
    };
    
    $scope.openNotifications = function(event){
      console.log("notifications opened");
      $scope.bildirimYok = false;
      notificationService();
      $mdDialog.show({
                  clickOutsideToClose: true,
                  locals:{dataToPass: $scope.parentScopeData},     
                  scope: $scope,
                  parent: angular.element(document.body),
                  preserveScope: true,           
                  templateUrl:'_notifications.html' ,
               });
              $scope.closeNotification = function(){
                $mdDialog.hide();
              };
    };
    
   /* $scope.changeItemStatus = function(event){
      $mdDialog.show({
                  clickOutsideToClose: true,
                  locals:{dataToPass: $scope.parentScopeData},     
                  scope: $scope,
                  parent: angular.element(document.body),
                  preserveScope: true,           
                  templateUrl:'_itemStatusChange.html' ,
               });
    };*/
    

    $scope.editItem = function(event, index){
      $scope.editItemName = $scope.items[index].name;
      $scope.editItemStatus = $scope.items[index].done;
      $scope.editItemDate = $scope.items[index].due_date;
      
      if($scope.editItemDate == null){
        $scope.editItemSaat;
      }
      else{
        var asd = $scope.editItemDate.substring(11,19);
        $scope.editItemSaat = new Date (new Date().toDateString() + ' ' + asd);
        
        //console.log($scope.editItemSaat);
      }
      
      var itemID = $scope.items[index].id;
      var listID = $scope.items[index].list_id;
      $mdDialog.show({
                  clickOutsideToClose: true,
                  locals:{dataToPass: $scope.parentScopeData},     
                  scope: $scope,
                  parent: angular.element(document.body),
                  preserveScope: true,           
                  templateUrl:'_editItem.html' ,
               });
        $scope.abortEditItem = function(){
          
          $mdDialog.hide();
        };
        
        $scope.saveEditedItem = function(){
          if($scope.editItemDate != $scope.items[index].due_date){
            var month;
            if($scope.editItemDate.getMonth()+1<10){
              var temp = $scope.editItemDate.getMonth()+1;
              month = '0' + temp;
            }
            else{
              month = month.getMonth()+1;
            }
            var date;
            if($scope.editItemDate.getDate()<10){
              date = '0' + $scope.editItemDate.getDate();
            }
            else{
              date = $scope.editItemDate.getDate();
            }
            
             var dueDate = $scope.editItemDate.getFullYear() + '-' +  month+ '-' + date;
          }
          else{
            console.log($scope.editItemDate);
            var dueDate = $scope.editItemDate.substring(0,10);
          }
          $scope.strtr = $scope.editItemSaat.toString();
          //console.log($scope.editItemSaat.getHours()+ " " + $scope.editItemSaat.getMinutes());
           var dueHour = $scope.strtr.substring(16,24);
           
           dueDate = dueDate + ' ' +dueHour;
           
          itemEditService(listID, itemID, $scope.editItemName, $scope.editItemStatus, dueDate, $mdDialog);
        };
    };
    
    
    $scope.openSettings = function(evet){
      console.log("settings pop up calisti");
      $scope.editUserName = $scope.User.name;
      $scope.editUserMail = $scope.User.email;
      $scope.editUserPassword = $scope.User.password;
       $mdDialog.show({
                  clickOutsideToClose: true,
                  locals:{dataToPass: $scope.parentScopeData},     
                  scope: $scope,
                  parent: angular.element(document.body),
                  preserveScope: true,           
                  templateUrl:'_settings.html' ,
               });
        $scope.abortSettings = function(){
          $mdDialog.hide();
        };
        $scope.saveEditedSettings = function(){
         editUserService($scope.editUserName, $scope.editUserMail, $scope.editUserPassword, $mdDialog);
        };
    };
  
    //----------------------------------------SERVICES    
    
    function notificationService(){
      console.log("notification service started");
      
      $http.get('/user/'+param1+'/notification').then(successCallback, errorCallback);
      function successCallback(response){
          console.log(response.data);
          
          $scope.notificationList = response.data;
          if($scope.notificationList.length == 0){
            $scope.bildirimYok = true;
          }
      }
      function errorCallback(error) {
          console.log(error);
      }
    }
    
    function editUserService(userName, userMail, userPassword, $mdDialog){
      console.log("edit user started");
       var params = {
          "name": "",
          "password": "",
          "email:": ""
      };
      params.name = userName;
      params.email = userMail;
      params.password = userPassword;
      $http.post('/user/'+ param1 + '/update', params).then(successCallback, errorCallback);
      function successCallback(response){
        console.log(response);
        userCredential();
        $mdDialog.hide();
      }
      function errorCallback(error){
        console.log(error);
      }
    }
    
    
    function getAllLists(){
      console.log("get all list started");
      $http.get('/user/'+param1+'/list').then(successCallback, errorCallback);
      function successCallback(response){
          console.log(response.data);
          $scope.lists = response.data;
          if($scope.lists.length == 0){
            $scope.items = [];
          }
          console.log("list");
          console.log($scope.lists);
      }
      function errorCallback(error) {
          console.log(error);
      }
    }
    
    function userCredential(){
      console.log("user info request");
      $http.get('/user/'+ param1).then(successCallback, errorCallback);
      function successCallback(response){
          console.log(response.data);
          $scope.firstName = response.data[0].name;
          $scope.User = response.data[0];
      }
      function errorCallback(error) {
          console.log(error);
      }
    }
    
    function getSecilenListItem(index){
      console.log("get secilen list items list id: " + index);
      $http.get('/user/'+ param1 +'/list/'+ index +'/item').then(successCallback, errorCallback);
      function successCallback(response){
          console.log(response.data);
          $scope.items = response.data;
      }
      function errorCallback(error) {
          console.log(error);
      }
    }
    
    function createNewList(listName, $mdDialog){
      console.log("create new list service started");
      var params = {
        "title": "",
        "user_id": ""
      };
      params.title = listName;
      params.user_id = param1;
      $http.post('/user/'+ param1 + '/list', params).then(successCallback, errorCallback);
      function successCallback(response){
        console.log(response);
        getAllLists();
        $mdDialog.hide();
      }
      function errorCallback(error){
        console.log(error);
      }
    }
    
    function createNewItem(itemName, listId, newItemDueDate, $mdDialog){
      var params = {
        "name": "",
        "done": false,
        "list_id": "",
        "due_date": ""
      };
      params.name = itemName;
      params.list_id = listId;
      params.due_date = newItemDueDate;
      $http.post('/user/'+param1+'/list/' + listId + '/item', params).then(successCallback, errorCallback);
      function successCallback(response){
        console.log(response);
        getSecilenListItem(listId);
        $mdDialog.hide();
      }
      function errorCallback(error){
        console.log(error);
      }
    }
    
    function itemEditService(listID, itemID, editItemName, editItemStatus, editItemDueDate, $mdDialog){
      var params = {
        "name": "",
        "done": "",
        "due_date": ""
      };
      params.name = editItemName;
      params.done = editItemStatus;
      params.due_date = editItemDueDate;
      $http.post('/user/'+ param1 + '/list/' + listID+ '/item/' + itemID + '/update', params).then(successCallback, errorCallback);
      function successCallback(response){
        console.log(response);
        getSecilenListItem(listID);
        $mdDialog.hide();
      }
      function errorCallback(error){
        console.log(error);
      }
    }
    
    function deleteListService(index, $mdDialog){
       $http.delete('/user/'+param1+'/list/'  + index).then(successCallback, errorCallback);
      function successCallback(response){
        getAllLists();
        $scope.items = [];
        $mdDialog.hide();
        console.log(response);
      }
      function errorCallback(error){
        console.log(error);
      }
    }
    
    function deleteItemService(index, mdDialog){
       $http.delete('/user/'+ param1+'/list/'+ $scope.gosterilenListId + '/item/' + index).then(successCallback, errorCallback);
      function successCallback(response){
        getSecilenListItem($scope.gosterilenListId);
        $mdDialog.hide();
        console.log(response);
      }
      function errorCallback(error){
        console.log(error);
      }
    }
    
   function editListService(listName, index){
      var params = {
        "title": "",
        "user_id": ""
      };
      params.title = listName;
      params.user_id = param1;
      $http.post('/user/' + param1 + '/list/' + index + '/update', params).then(successCallback, errorCallback);
      function successCallback(response){
        getAllLists();
        //$scope.items = [];
        console.log(response);
      }
      function errorCallback(error){
        console.log(error);
      }
    }
});
