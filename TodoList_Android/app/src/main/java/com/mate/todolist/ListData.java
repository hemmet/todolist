package com.mate.todolist;

/**
 * Created by Mert on 21.07.2017.
 */

public class ListData {
    private String creation;
    private String due="";
    private String listName;
    private String id;
    private String userID;

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUserID() {

        return userID;

    }

    public void setCreation(String creation) {
        this.creation = creation;
    }

    public void setDue(String due) {
        this.due = due;
    }

    public void setListName(String listName) {
        this.listName = listName;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {

        return id;
    }

    public String getListName() {

        return listName;
    }

    public String getDue() {

        return due;
    }

    public String getCreation() {

        return creation;
    }
}
