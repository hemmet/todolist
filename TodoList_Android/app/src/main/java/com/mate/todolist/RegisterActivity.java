package com.mate.todolist;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {
    private final static String SIGNUP_URL = "http://mysterious-everglades-87230.herokuapp.com/user";
    private String email_text;
    private String username_text;
    private String password_text;
    private String retype_text;

    private boolean emailFlag = false;
    private boolean usernameFlag = false;
    private boolean passwordFlag = false;
    private boolean retypeFlag = false;

    private EditText Email;
    private EditText Username;
    private EditText Password;
    private EditText Retype;
    private TextView Match;
    ProgressDialog pdLoading;
    RequestQueue queue;

    Button Signup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        queue = Volley.newRequestQueue(this);

        Email = (EditText) findViewById(R.id.email);
        Username = (EditText) findViewById(R.id.username);
        Password = (EditText) findViewById(R.id.password);
        Retype = (EditText) findViewById(R.id.retype);
        Match = (TextView) findViewById(R.id.matching);
        Signup = (Button) findViewById(R.id.sign_ok);

        Signup.setEnabled(false);
        Signup.setAlpha(.25f);

        Match.setVisibility(View.INVISIBLE);

        Retype.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().equals(password_text)) {
                    Match.setVisibility(View.VISIBLE);
                    retypeFlag = false;
                } else {
                    Match.setVisibility(View.INVISIBLE);
                    retypeFlag = true;
                }
                if (usernameFlag && passwordFlag && emailFlag && retypeFlag) {
                    Signup.setEnabled(true);
                    Signup.setAlpha(1f);
                } else {
                    Signup.setEnabled(false);
                    Signup.setAlpha(0.25f);
                }
            }
        });
        Email.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                email_text = Email.getText().toString();
                if (s.toString().isEmpty()) emailFlag = false;
                else emailFlag = true;
                if (usernameFlag && passwordFlag && emailFlag && retypeFlag) {
                    Signup.setEnabled(true);
                    Signup.setAlpha(1f);
                } else {
                    Signup.setEnabled(false);
                    Signup.setAlpha(0.25f);
                }
            }
        });
        Username.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                username_text = Username.getText().toString();
                if (s.toString().isEmpty()) usernameFlag = false;
                else usernameFlag = true;
                if (usernameFlag && passwordFlag && emailFlag && retypeFlag) {
                    Signup.setEnabled(true);
                    Signup.setAlpha(1f);
                } else {
                    Signup.setEnabled(false);
                    Signup.setAlpha(0.25f);
                }
            }
        });
        Password.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                password_text = Password.getText().toString();
                if (s.toString().isEmpty()) passwordFlag = false;
                else passwordFlag = true;

                if (usernameFlag && passwordFlag && emailFlag && retypeFlag) {
                    Signup.setEnabled(true);
                    Signup.setAlpha(1f);
                } else {
                    Signup.setEnabled(false);
                    Signup.setAlpha(0.25f);
                }
            }
        });


        Signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Map<String, String> postData = new HashMap<String, String>();
                postData.put("email", email_text);
                postData.put("name", username_text);
                postData.put("password", password_text);
                pdLoading = new ProgressDialog(RegisterActivity.this);
                pdLoading.setMessage("\tSigning up...");
                pdLoading.setCancelable(false);
                pdLoading.show();
                JSONObject jso = new JSONObject(postData);
                CustomJsonArrayRequest req = new CustomJsonArrayRequest(Request.Method.POST, SIGNUP_URL, jso,new Response.Listener<JSONArray>(){

                    @Override
                    public void onResponse(JSONArray response){
                        pdLoading.dismiss();

                        if (response.toString().contains("Failure")) Toast.makeText(RegisterActivity.this, "User Exists", Toast.LENGTH_LONG).show();
                        else {

                                Intent changeToMainActivity = new Intent(RegisterActivity.this, MainActivity.class);
                                startActivity(changeToMainActivity);

                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pdLoading.dismiss();
                        System.out.println("onErrorResponse()");
                        System.out.println(error.toString());


                    }
                });


                System.out.println("After the request is made");

                queue.add(req);

            }

        });
    }
}
