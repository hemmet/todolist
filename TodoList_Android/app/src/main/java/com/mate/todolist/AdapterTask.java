package com.mate.todolist;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Mert on 21.07.2017.
 */
public class AdapterTask extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final static String DELETE_TASK_URL = "http://mysterious-everglades-87230.herokuapp.com/user/";
    private Context context;
    private LayoutInflater inflater;
    List<TaskData> data= Collections.emptyList();
    TaskData current;
    int currentPos=0;
    TaskHolder taskHolder;

    String list_id;
    String user_id;


    public AdapterTask(Context context, List<TaskData> data , String list_id, String user_id) {
        this.context=context;
        inflater= LayoutInflater.from(context);
        this.data=data;
        this.list_id = list_id;
        this.user_id = user_id;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.task_elements, parent,false);
        taskHolder=new TaskHolder(view);
        return taskHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        taskHolder = (TaskHolder) holder;
        current=data.get(position);
        taskHolder.taskDescription.setText(current.getTaskName());
        if (current.getDone().equals("true"))
            taskHolder.taskDescription.setPaintFlags(taskHolder.taskDescription.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        else
            taskHolder.taskDescription.setPaintFlags(0);
        taskHolder.due.setText(current.getDue());



    }

    @Override
    public int getItemCount() {
        return data.size();
    }
    class TaskHolder extends RecyclerView.ViewHolder{
        TextView taskDescription;
        TextView due;
        Button delete;
        Button change;

        public TaskHolder(View itemView) {
            super(itemView);
            taskDescription= (TextView) itemView.findViewById(R.id.taskName);
            due= (TextView) itemView.findViewById(R.id.due);
            delete = (Button) itemView.findViewById(R.id.taskDel);
            change = (Button) itemView.findViewById(R.id.change);

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Map<String, String> postData = new HashMap<>();
                    //postData.put("listID", data.get(getAdapterPosition()).getListID());
                    //postData.put("userID", current.getUserID());
                    TaskData currentData = data.get(getAdapterPosition());
                    AdapterTask.deleteTask log = new AdapterTask.deleteTask(currentData.getItemID());
                    log.execute();

                }
            });
            change.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    TaskData currentData = data.get(getAdapterPosition());
                    Intent changeToChangeTask = new Intent(context, ChangeTask.class);
                    changeToChangeTask.putExtra("list_id", currentData.getListID());
                    changeToChangeTask.putExtra("user_id", user_id);
                    changeToChangeTask.putExtra("name", currentData.getTaskName());
                    changeToChangeTask.putExtra("item_id", currentData.getItemID());
                    context.startActivity(changeToChangeTask);
                }
            });




        }
    }

    private class  deleteTask extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(context);
        HttpURLConnection conn;
        URL url = null;
        JSONObject postData;

        String item_id;

        public deleteTask(String item_id) {
            if (!item_id.isEmpty()) {
                this.item_id = item_id;
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tDeleting List...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }
        @Override
        protected String doInBackground(String... params) {
            try {

                url = new URL(DELETE_TASK_URL+ user_id +"/list/"+list_id+"/item/"+item_id);
                System.out.println(url);
                conn = (HttpURLConnection) url.openConnection();
                //conn.setReadTimeout(15000);
                //conn.setConnectTimeout(10000);
                conn.setRequestMethod("DELETE");

                //conn.setDoOutput(true);
               // conn.setDoInput(true);

                /*if (this.postData != null) {
                    OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
                    writer.write(postData.toString());
                    writer.flush();
                }
*/
                conn.getResponseCode();
/*
                if (statusCode ==  HttpURLConnection.HTTP_OK) {

                    InputStream inputStream = new BufferedInputStream(conn.getInputStream());
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

                    String line;
                    StringBuilder result = new StringBuilder();
                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    // Pass data to onPostExecute method
                    return (result.toString());

                } else {

                    return ("unsuccessful");
                }
*/          return "successful";

            } catch (IOException e) {
                e.printStackTrace();
                return e.toString();
            } finally {
                conn.disconnect();
            }

        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

            pdLoading.dismiss();
            pdLoading.dismiss();

           // try {

               // JSONArray jArray = new JSONArray(result);

                // Extract data from json and store into ArrayList as class objects
                //JSONObject json_data = jArray.getJSONObject(0);
                if (!result.equals("successful")) {
                    Toast.makeText(context, "Failed to delete", Toast.LENGTH_LONG).show();

                }
                else {
                    Intent refresh = new Intent(context,TaskActivity.class);
                    refresh.putExtra("list_id",list_id);
                    refresh.putExtra("user_id", user_id);
                    context.startActivity(refresh);
                }



            /*} catch (JSONException e) {
                Toast.makeText(context, e.toString(), Toast.LENGTH_LONG).show();
            }*/

        }


    }
}
