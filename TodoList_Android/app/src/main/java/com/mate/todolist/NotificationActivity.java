package com.mate.todolist;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class NotificationActivity extends AppCompatActivity {
    ProgressDialog pdLoading;
    RequestQueue queue;
    Bundle extras;
    final static private String NOTIF_URL = "http://mysterious-everglades-87230.herokuapp.com/user/";

    @Override
    public void onBackPressed() {
        Intent changeToMainActivity = new Intent(NotificationActivity.this, ListActivity.class);
        changeToMainActivity.putExtra("user_id",extras.getString("user_id"));
        startActivity(changeToMainActivity);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        queue = Volley.newRequestQueue(this);
        extras = getIntent().getExtras();

        pdLoading = new ProgressDialog(NotificationActivity.this);
        pdLoading.setMessage("\tLoading...");
        pdLoading.setCancelable(false);
        pdLoading.show();
        System.out.println("URLLLLLLLL" + NOTIF_URL+extras.getString("user_id")+"/notification");
        CustomJsonArrayRequest req = new CustomJsonArrayRequest(Request.Method.GET, NOTIF_URL+extras.getString("user_id")+"/notification", null,new Response.Listener<JSONArray>(){

            @Override
            public void onResponse(JSONArray response){
                pdLoading.dismiss();

                if (response.toString().contains("Failure")) Toast.makeText(NotificationActivity.this, "User Exists", Toast.LENGTH_LONG).show();
                else {
                    ArrayList<JSONObject> list=getArrayListFromJSONArray(response);
                    ListView listView = (ListView) findViewById(R.id.listview);
                    ListAdapter adapter=new ListAdapter(NotificationActivity.this,R.layout.list_view,R.id.txtid,list);
                    listView.setAdapter(adapter);
                    }



                }
            }
        , new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                pdLoading.dismiss();
                System.out.println("onErrorResponse()");
                System.out.println(error.toString());


            }
        });


        System.out.println("After the request is made");

        queue.add(req);




    }
    private ArrayList<JSONObject> getArrayListFromJSONArray(JSONArray jsonArray){

        ArrayList<JSONObject> aList=new ArrayList<JSONObject>();

        try {

            if (jsonArray != null) {

                for (int i = 0; i < jsonArray.length(); i++) {

                    aList.add(jsonArray.getJSONObject(i));

                }

            }

        }catch (JSONException je){je.printStackTrace();}

        return  aList;

    }

}


