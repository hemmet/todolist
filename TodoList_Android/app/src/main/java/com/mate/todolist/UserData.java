package com.mate.todolist;

/**
 * Created by Mert on 09.08.2017.
 */

public class UserData {
    private String user_id;
    private String mail;
    private String password;

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public void setPassword(String password) {
        this.password = password;
    }



    public String getPassword() {
        return password;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getMail() {
        return mail;
    }


}
