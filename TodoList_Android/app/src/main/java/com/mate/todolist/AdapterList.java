package com.mate.todolist;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Mert on 21.07.2017.
 */
public class AdapterList extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final static String DELETE_URL = "http://mysterious-everglades-87230.herokuapp.com/user/";
    private Context context;
    private LayoutInflater inflater;
    List<ListData> data= Collections.emptyList();
    ListData current;
    ListHolder listHolder;
    int currentPos=0;

    public AdapterList(Context context, List<ListData> data) {
        this.context=context;
        inflater= LayoutInflater.from(context);
        this.data=data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.list_elements, parent,false);
        listHolder=new ListHolder(view);
        return listHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        listHolder = (ListHolder) holder;
        current=data.get(position);
        listHolder.listName.setText(current.getListName());
        listHolder.creation.setText(current.getCreation());

    }

    @Override
    public int getItemCount() {
        return data.size();
    }
    class ListHolder extends RecyclerView.ViewHolder{
        TextView listName;
        TextView due;
        TextView creation;
        Button delete;
        Button change;
        public ListHolder(View itemView) {
            super(itemView);
            listName= (TextView) itemView.findViewById(R.id.listName);
            creation = (TextView) itemView.findViewById(R.id.creation_date);
            delete = (Button) itemView.findViewById(R.id.listDel);
            change=(Button) itemView.findViewById(R.id.change);

            listName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ListData currentData = data.get(getAdapterPosition());
                    Intent changeToTaskActivity = new Intent(context, TaskActivity.class);
                    changeToTaskActivity.putExtra("list_id", currentData.getId());
                    changeToTaskActivity.putExtra("user_id", currentData.getUserID());
                    context.startActivity(changeToTaskActivity);
                }
            });

            change.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ListData currentData = data.get(getAdapterPosition());
                    Intent changeToChange = new Intent(context, ChangeList.class);
                    changeToChange.putExtra("list_id", currentData.getId());
                    changeToChange.putExtra("user_id", currentData.getUserID());
                    changeToChange.putExtra("name", currentData.getListName());
                    context.startActivity(changeToChange);
                }
            });
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ListData currentData = data.get(getAdapterPosition());
                    AdapterList.deleteTask log = new AdapterList.deleteTask(currentData.getId(), currentData.getUserID());
                    log.execute();
                }
            });
        }
    }

    private class  deleteTask extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(context);
        HttpURLConnection conn;
        URL url = null;
        String ID;
        String userID;

        public deleteTask(String ID,String userID) {
            if (!ID.isEmpty()) {
                this.ID = ID;
                this.userID = userID;
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tDeleting List...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }
        @Override
        protected String doInBackground(String... params) {
            try {

                url = new URL(DELETE_URL +userID +"/list/" + ID );
                System.out.println(url.toString());
                conn = (HttpURLConnection) url.openConnection();
               //conn.setReadTimeout(15000);
               // conn.setConnectTimeout(10000);
                conn.setRequestMethod("DELETE");

                //conn.setDoOutput(true);
               // conn.setDoInput(true);

                /*if (this.postData != null) {
                    OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
                    writer.write(postData.toString());
                    writer.flush();
                }*/

                conn.getResponseCode();

                //if (statusCode ==  HttpURLConnection.HTTP_OK) {*/

                   /* InputStream inputStream = new BufferedInputStream(conn.getInputStream());
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

                    String line;
                    StringBuilder result = new StringBuilder();
                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    // Pass data to onPostExecute method*/
                  /*  return ("successful") ;

                } else {

                    return ("unsuccessful");
                }*/
                return "successful" ;

            } catch (IOException e) {
                e.printStackTrace();
                return e.toString();
            } finally {
                conn.disconnect();
            }

        }


        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

            pdLoading.dismiss();
            pdLoading.dismiss();
            System.out.println(result);

            //try {

                //JSONArray jArray = new JSONArray(result);

                // Extract data from json and store into ArrayList as class objects
                //JSONObject json_data = jArray.getJSONObject(0);
                if (!result.equals("successful")) {
                    Toast.makeText(context, "failed to delete", Toast.LENGTH_LONG).show();

                }
                else {
                    Intent refresh = new Intent(context,ListActivity.class);
                    refresh.putExtra("user_id", userID);
                    context.startActivity(refresh);
                }



            /*} catch (JSONException e) {
                Toast.makeText(context, e.toString(), Toast.LENGTH_LONG).show();
            }*/

        }


    }
}
