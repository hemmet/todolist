package com.mate.todolist;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TaskActivity extends AppCompatActivity {
    private final static String RETRIEVE_TASKS_URL = "http://mysterious-everglades-87230.herokuapp.com/user/";
    private RecyclerView taskView;
    private AdapterTask taskAdapter;
    Bundle extras;

    @Override
    public void onBackPressed() {
        Intent changeToListActivity = new Intent(TaskActivity.this,ListActivity.class);
        changeToListActivity.putExtra("user_id", extras.getString("user_id"));
        startActivity(changeToListActivity);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);
        extras= getIntent().getExtras();
        Map<String, String> postData = new HashMap<String, String>();
        //postData.put("listID", extras.getString("listID"));
        TaskActivity.loadTasks load = new TaskActivity.loadTasks();
        load.execute();

        Button addTask = (Button)findViewById(R.id.addTask);
        addTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent changeToAddTask = new Intent(TaskActivity.this, AddTask.class);
                changeToAddTask.putExtra("list_id", extras.getString("list_id"));
                changeToAddTask.putExtra("user_id", extras.getString("user_id"));
                startActivity(changeToAddTask);
            }
        });
    }
    private class  loadTasks extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(TaskActivity.this);
        HttpURLConnection conn;
        URL url = null;
        //JSONObject postData;

        loadTasks() {//Map<String, String> postData) {
           /* if (postData != null) {
                this.postData = new JSONObject(postData);
            }*/
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading Tasks...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }
        @Override
        protected String doInBackground(String... params) {
            try {

                url = new URL(RETRIEVE_TASKS_URL+extras.getString("user_id")+ "/list/" +extras.getString("list_id") + "/item");
                System.out.println(url.toString());
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(10000);
                conn.setRequestMethod("GET");

                //conn.setDoOutput(true);
               conn.setDoInput(true);
                /*if (this.postData != null) {
                    OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
                    writer.write(postData.toString());
                    writer.flush();
                }*/

                int statusCode = conn.getResponseCode();

                if (statusCode ==  HttpURLConnection.HTTP_OK) {

                    InputStream inputStream = new BufferedInputStream(conn.getInputStream());
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

                    String line;
                    StringBuilder result = new StringBuilder();
                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    // Pass data to onPostExecute method
                    return (result.toString());

                } else {

                    return ("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return e.toString();
            } finally {
                conn.disconnect();
            }

        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

            pdLoading.dismiss();
            List<TaskData> data=new ArrayList<>();
            pdLoading.dismiss();

            try {

                JSONArray jArray = new JSONArray(result);

                for(int i=0;i<jArray.length();i++){
                    JSONObject json_data = jArray.getJSONObject(i);
                    TaskData taskData = new TaskData();
                    taskData.setListID(json_data.getString("list_id"));
                    taskData.setItemID(json_data.getString("id"));
                    //taskData.setUserID(json_data.getString("userID"));
                    taskData.setDue(json_data.getString("due_date"));
                    taskData.setTaskName(json_data.getString("name"));
                    taskData.setDone(json_data.getString("done"));
                    data.add(taskData);
                }

                // Setup and Handover data to recyclerview
                taskView = (RecyclerView)findViewById(R.id.listoftasks);
                taskAdapter = new AdapterTask(TaskActivity.this, data, extras.getString("list_id"), extras.getString("user_id"));
                taskView.setAdapter(taskAdapter);
                taskView.setLayoutManager(new LinearLayoutManager(TaskActivity.this));




            } catch (JSONException e) {
                Toast.makeText(TaskActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }


    }
}
