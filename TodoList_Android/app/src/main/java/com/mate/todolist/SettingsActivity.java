package com.mate.todolist;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SettingsActivity extends AppCompatActivity {
    Bundle extras;
    EditText UserEdit;
    EditText PasswordEdit;
    EditText MailEdit;
    ProgressDialog pdLoading;
    final static String UPDATEUSR_URL ="http://mysterious-everglades-87230.herokuapp.com/user/";
    RequestQueue queue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        extras = getIntent().getExtras();
        queue = Volley.newRequestQueue(this);

        UserEdit =  (EditText) findViewById(R.id.id_edit);
        UserEdit.setText(extras.getString("username"));
        PasswordEdit =  (EditText) findViewById(R.id.password_edit);
        PasswordEdit.setText(extras.getString("password"));
        MailEdit =  (EditText) findViewById(R.id.mail_edit);
        MailEdit.setText(extras.getString("email"));

        Button Done = (Button) findViewById(R.id.done);
        Done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Map<String, String> postData = new HashMap<>();
                postData.put("email", MailEdit.getText().toString());
                postData.put("name", UserEdit.getText().toString());
                postData.put("password", PasswordEdit.getText().toString());

                pdLoading = new ProgressDialog(SettingsActivity.this);
                pdLoading.setMessage("\tChanging...");
                pdLoading.setCancelable(false);
                pdLoading.show();
                JSONObject jso = new JSONObject(postData);
                CustomJsonArrayRequest req = new CustomJsonArrayRequest(Request.Method.POST, UPDATEUSR_URL+extras.getString("user_id")+"/update", jso,new Response.Listener<JSONArray>(){

                    @Override
                    public void onResponse(JSONArray response){
                        pdLoading.dismiss();

                        if (response.toString().contains("Failure")) Toast.makeText(SettingsActivity.this, "User Exists", Toast.LENGTH_LONG).show();
                        else {
                            try {
                                Intent changeToListActivity = new Intent(SettingsActivity.this, ListActivity.class);
                                changeToListActivity.putExtra("user_id", response.getJSONObject(0).getString("id"));
                                changeToListActivity.putExtra("username", response.getJSONObject(0).getString("name"));
                                changeToListActivity.putExtra("password", response.getJSONObject(0).getString("password"));
                                changeToListActivity.putExtra("email", response.getJSONObject(0).getString("email"));
                                startActivity(changeToListActivity);
                            }
                            catch (JSONException e){
                                e.printStackTrace();
                            }

                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pdLoading.dismiss();
                        System.out.println("onErrorResponse()");
                        System.out.println(error.toString());


                    }
                });


                System.out.println("After the request is made");

                queue.add(req);
            }
        });
    }
}
