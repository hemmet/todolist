package com.mate.todolist;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ChangeList extends AppCompatActivity {
    Bundle extras;
    EditText TitleEdit;
    ProgressDialog pdLoading;
    final static String UPDATELST_URL ="http://mysterious-everglades-87230.herokuapp.com/user/";
    RequestQueue queue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_list);
        extras = getIntent().getExtras();
        queue = Volley.newRequestQueue(this);
        TitleEdit =  (EditText) findViewById(R.id.titleEdit);
        TitleEdit.setText(extras.getString("name"));

        Button Done = (Button) findViewById(R.id.done);
        Done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Map<String, String> postData = new HashMap<>();
                postData.put("title", TitleEdit.getText().toString());
                postData.put("user_id", extras.getString("user_id"));
                pdLoading = new ProgressDialog(ChangeList.this);
                pdLoading.setMessage("\tChanging...");
                pdLoading.setCancelable(false);
                pdLoading.show();
                JSONObject jso = new JSONObject(postData);
                CustomJsonArrayRequest req = new CustomJsonArrayRequest(Request.Method.POST, UPDATELST_URL + extras.getString("user_id") +"/list/"+extras.getString("list_id")+ "/update", jso, new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        pdLoading.dismiss();
                        System.out.println(response.toString());
                        if (response.toString().contains("Failure"))
                            Toast.makeText(ChangeList.this, "Failure", Toast.LENGTH_LONG).show();
                        else {
                            try {
                                Intent changeToListActivity = new Intent(ChangeList.this, ListActivity.class);
                                changeToListActivity.putExtra("user_id", response.getJSONObject(0).getString("user_id"));
                                startActivity(changeToListActivity);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pdLoading.dismiss();
                        System.out.println("onErrorResponse()");
                        System.out.println(error.toString());


                    }
                });


                System.out.println("After the request is made");

                queue.add(req);
            }
        });


    }
}
