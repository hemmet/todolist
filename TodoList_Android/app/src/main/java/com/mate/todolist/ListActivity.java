package com.mate.todolist;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListActivity extends AppCompatActivity {
    private final static String RETRIEVE_LISTS_URL = "http://mysterious-everglades-87230.herokuapp.com/user/";
    private RecyclerView listView;
    private AdapterList listAdapter;
    Bundle extras;

    @Override
    public void onBackPressed() {}


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
       // Map<String, String> postData = new HashMap<String, String>();
        //extras = getIntent().getExtras();
        //postData.put("userID", extras.getString("id"));
        //postData.put("","1");
        //loadLists load = new loadLists(postData);
        loadLists load = new loadLists();
        load.execute();

        Button addList = (Button)findViewById(R.id.addList);
        addList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent changeToAddList = new Intent(ListActivity.this, AddList.class);
                changeToAddList.putExtra("user_id", extras.getString("user_id"));
                startActivity(changeToAddList);
            }
        });
        Button Logout = (Button)findViewById(R.id.logout);
        Logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent changeLogin = new Intent(ListActivity.this, MainActivity.class);
                startActivity(changeLogin);
            }
        });
        Button Settings = (Button)findViewById(R.id.settings);
        Settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent changeSettings= new Intent(ListActivity.this, SettingsActivity.class);
                changeSettings.putExtra("email", extras.getString("email"));
                changeSettings.putExtra("user_id", extras.getString("user_id"));
                changeSettings.putExtra("username", extras.getString("username"));
                changeSettings.putExtra("user_id", extras.getString("user_id"));
                startActivity(changeSettings);
            }
        });
        Button Notifications = (Button)findViewById(R.id.notif);
        Notifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent changeSettings= new Intent(ListActivity.this, NotificationActivity.class);
                changeSettings.putExtra("email", extras.getString("email"));
                changeSettings.putExtra("user_id", extras.getString("user_id"));
                changeSettings.putExtra("username", extras.getString("username"));
                changeSettings.putExtra("user_id", extras.getString("user_id"));
                startActivity(changeSettings);
            }
        });
    }
    private class  loadLists extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(ListActivity.this);
        HttpURLConnection conn;
        URL url = null;
        JSONObject postData;

        public loadLists(){//Map<String, String> )postData) {
            /*if (postData != null) {
                this.postData = new JSONObject(postData);
            }*/
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading Lists...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }
        @Override
        protected String doInBackground(String... params) {
            try {
                extras = getIntent().getExtras();
                url = new URL(RETRIEVE_LISTS_URL+extras.get("user_id")+"/list" +
                        "");
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(10000);
                conn.setRequestMethod("GET");

                //conn.setDoOutput(true);
                conn.setDoInput(true);

                /*if (this.postData != null) {
                    OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
                    writer.write(postData.toString());
                    writer.flush();
                }
*/
                int statusCode = conn.getResponseCode();

                if (statusCode ==  HttpURLConnection.HTTP_OK) {

                    InputStream inputStream = new BufferedInputStream(conn.getInputStream());
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

                    String line;
                    StringBuilder result = new StringBuilder();
                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    // Pass data to onPostExecute method
                    return (result.toString());

                } else {

                    return ("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return e.toString();
            } finally {
                conn.disconnect();
            }

        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

            pdLoading.dismiss();
            List<ListData> data=new ArrayList<>();
            pdLoading.dismiss();

            try {

                JSONArray jArray = new JSONArray(result);

                for(int i=0;i<jArray.length();i++){
                    JSONObject json_data = jArray.getJSONObject(i);
                    ListData listData = new ListData();
                    listData.setId(json_data.getString("id"));
                    listData.setListName(json_data.getString("title"));
                    listData.setUserID(json_data.getString("user_id"));
                    listData.setCreation(json_data.getString("created_at"));
                   //listData.setDue(json_data.getString("due"));
                    data.add(listData);
                }

                // Setup and Handover data to recyclerview
                listView = (RecyclerView)findViewById(R.id.listoflists);
                listAdapter = new AdapterList(ListActivity.this, data);
                listView.setAdapter(listAdapter);
                listView.setLayoutManager(new LinearLayoutManager(ListActivity.this));




            } catch (JSONException e) {
                Toast.makeText(ListActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }


    }
}
