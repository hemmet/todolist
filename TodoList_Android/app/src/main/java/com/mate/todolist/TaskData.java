package com.mate.todolist;

/**
 * Created by Mert on 21.07.2017.
 */
public class TaskData {
    private String due  = "xxx";
    private String taskName;
    private String listID;
    private String itemID;
    private String done;


    public void setDone(String done) {
        this.done = done;
    }


    public String getDone() {
        return done;
    }



    public void setItemID(String itemID) {
        this.itemID = itemID;
    }

    public String getItemID() {

        return itemID;
    }
    //private String userID;

    public void setDue(String due) {
        this.due = due.equals("null") ? "" :due;
    }

  /*  public void setUserID(String userID) {
        this.userID = userID;
    }*/

   public void setListID(String listID) {
        this.listID = listID;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getDue() {
        return due;
    }

    public String getTaskName() {
        return taskName;
    }

    public String getListID() {
        return listID;
    }

    /*public String getUserID() {
        return userID;
    }*/


}
