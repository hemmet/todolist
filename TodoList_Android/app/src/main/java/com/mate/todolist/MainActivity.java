package com.mate.todolist;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.media.session.PlaybackStateCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    private final static String LOGIN_URL = "http://mysterious-everglades-87230.herokuapp.com/login";
    private String username_text;
    private String password_text;
    private EditText Username;
    private EditText Password;

    private boolean usernameFlag =false;
    private boolean passwordFlag =false;

    Button Login;

    ProgressDialog pdLoading;

    RequestQueue queue;
    @Override
    public void onBackPressed() {}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        queue = Volley.newRequestQueue(this);
        setContentView(R.layout.activity_main);
        Username = (EditText) findViewById(R.id.username);
        Password = (EditText) findViewById(R.id.password);
        Login = (Button)findViewById(R.id.login);


        Username.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {

            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                username_text = Username.getText().toString();
                if(s.toString().isEmpty()) usernameFlag = false;
                else usernameFlag = true;

                if (usernameFlag && passwordFlag) {
                    Login.setEnabled(true);
                    Login.setAlpha(1f);
                }
                else  {
                    Login.setEnabled(false);
                    Login.setAlpha(.25f);
                }

            }
        });
        Password.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {

            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                password_text = Password.getText().toString();
                if(s.toString().isEmpty()) passwordFlag = false;
                else passwordFlag = true;

                if (usernameFlag && passwordFlag) {
                    Login.setEnabled(true);
                    Login.setAlpha(1f);
                }
                else  {
                    Login.setEnabled(false);
                    Login.setAlpha(.25f);
                }


            }
        });



        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pdLoading = new ProgressDialog(MainActivity.this);
                pdLoading.setMessage("\tLogging In...");
                pdLoading.setCancelable(false);
                pdLoading.show();
                Map<String, String> postData = new HashMap<>();
                postData.put("name", username_text);
                postData.put("password", password_text);
                JSONObject jso = new JSONObject(postData);
                CustomJsonArrayRequest req = new CustomJsonArrayRequest(Request.Method.POST, LOGIN_URL, jso,new Response.Listener<JSONArray>(){

                    @Override
                    public void onResponse(JSONArray response){
                        pdLoading.dismiss();
                        if (response.toString().equals("[]")) Toast.makeText(MainActivity.this, "Wrong credentials", Toast.LENGTH_LONG).show();
                        else {

                            try {
                                MyFirebaseInstanceIDService fcmIDService = new MyFirebaseInstanceIDService();
                                fcmIDService.sendRegistrationToServer(FirebaseInstanceId.getInstance().getToken(), response.getJSONObject(0).getString("id"),queue,MainActivity.this);
                                System.out.println("TOKEN " +  FirebaseInstanceId.getInstance().getToken());
                                Intent changeToListActivity = new Intent(MainActivity.this, ListActivity.class);
                                changeToListActivity.putExtra("user_id", response.getJSONObject(0).getString("id"));
                                changeToListActivity.putExtra("username", response.getJSONObject(0).getString("name"));
                                changeToListActivity.putExtra("password", response.getJSONObject(0).getString("password"));
                                changeToListActivity.putExtra("email", response.getJSONObject(0).getString("email"));
                                startActivity(changeToListActivity);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pdLoading.dismiss();
                        System.out.println("onErrorResponse()");
                        System.out.println(error.toString());


                    }
                });


                System.out.println("After the request is made");
// Add the request to the RequestQueue.
                queue.add(req);




            }
        });

        Button Signup = (Button)findViewById(R.id.signup);
        Signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               Intent changeToRegisterActivity = new Intent(MainActivity.this,RegisterActivity.class);
                startActivity(changeToRegisterActivity);
            }
        });

    }



}
