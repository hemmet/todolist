package com.mate.todolist;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class AddTask extends AppCompatActivity implements
        View.OnClickListener{
    private final static String ADDTASK_URL = "http://mysterious-everglades-87230.herokuapp.com/user/";
    private String taskDescription;
    private String dateText="Date";
    private String timeText="Time";


    private EditText DateText;
    private EditText TimeText;
    private Button bDatePicker;
    private Button bTimePicker;
    private Button AddButton;
    private EditText TaskDescription;

    private boolean checkFlag = false;
    private boolean nameFlag = false;
    private boolean dateFlag = false;
    private boolean timeFlag = false;

    ProgressDialog pdLoading;
    RequestQueue queue;

    private int mYear, mMonth, mDay, mHour, mMinute;
    Bundle extras;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        queue = Volley.newRequestQueue(this);
        setContentView(R.layout.activity_add_task);

        TaskDescription = (EditText) findViewById(R.id.taskDesc);
        CheckBox DueCheck = (CheckBox) findViewById(R.id.dueCheck1);

        DateText = (EditText) findViewById(R.id.textDate1);
        TimeText = (EditText) findViewById(R.id.textTime1);
        bDatePicker = (Button) findViewById(R.id.bSetDate1);
        bTimePicker = (Button) findViewById(R.id.bSetTime1);

        AddButton = (Button) findViewById(R.id.addTask);
        AddButton.setAlpha(.25f);
        AddButton.setEnabled(false);



        TaskDescription.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {

            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                taskDescription = s.toString();
                if(s.toString().isEmpty()) nameFlag = false;
                else nameFlag = true;

                if(nameFlag){
                    if(checkFlag){
                        if(dateFlag&&timeFlag){
                            AddButton.setEnabled(true);
                            AddButton.setAlpha(1f);
                        }
                        else{
                            AddButton.setEnabled(false);
                            AddButton.setAlpha(.25f);
                        }
                    }else{
                        AddButton.setEnabled(true);
                        AddButton.setAlpha(1f);
                    }
                }
                else{
                    AddButton.setEnabled(false);
                    AddButton.setAlpha(.25f);
                }
            }
        });
        DateText.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {

            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                dateText = s.toString();
                if(s.toString().equals("Date")) dateFlag = false;
                else dateFlag = true;

                if(nameFlag){
                    if(checkFlag){
                        if(dateFlag&&timeFlag){
                            AddButton.setEnabled(true);
                            AddButton.setAlpha(1f);
                        }
                        else{
                            AddButton.setEnabled(false);
                            AddButton.setAlpha(.25f);
                        }
                    }else{
                        AddButton.setEnabled(true);
                        AddButton.setAlpha(1f);
                    }
                }
                else{
                    AddButton.setEnabled(false);
                    AddButton.setAlpha(.25f);
                }
            }
        });

        TimeText.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {

            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                timeText = TimeText.getText().toString();
                if(s.toString().equals("Time")) timeFlag = false;
                else timeFlag = true;

                if(nameFlag){
                    if(checkFlag){
                        if(dateFlag&&timeFlag){
                            AddButton.setEnabled(true);
                            AddButton.setAlpha(1f);
                        }
                        else{
                            AddButton.setEnabled(false);
                            AddButton.setAlpha(.25f);
                        }
                    }else{
                        AddButton.setEnabled(true);
                        AddButton.setAlpha(1f);
                    }
                }
                else{
                    AddButton.setEnabled(false);
                    AddButton.setAlpha(.25f);
                }
            }
        });



        AddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String arr[] =dateText.split("-");
                dateText = arr[2]+"-"+arr[1] + "-"+arr[0];
                String due = dateText + " " + timeText +":00";
                System.out.println(due);
                extras = getIntent().getExtras();
                Map<String, String> postData = new HashMap<>();
                postData.put("name", taskDescription);
                postData.put("due_date", due);
                postData.put("done","false");

                pdLoading = new ProgressDialog(AddTask.this);
                pdLoading.setMessage("\tAdding List...");
                pdLoading.setCancelable(false);
                pdLoading.show();

                JSONObject jso = new JSONObject(postData);
                System.out.println(jso.toString());
                CustomJsonArrayRequest req = new CustomJsonArrayRequest(Request.Method.POST, ADDTASK_URL + extras.getString("user_id") + "/list/" + extras.getString("list_id")+"/item/", jso,new Response.Listener<JSONArray>(){

                    @Override
                    public void onResponse(JSONArray response){

                        Intent changeToTaskActivity = new Intent(AddTask.this, TaskActivity.class);
                        try {
                            changeToTaskActivity.putExtra("list_id", response.getJSONObject(0).getString("list_id"));
                            changeToTaskActivity.putExtra("user_id", extras.getString("user_id"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        startActivity(changeToTaskActivity);
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pdLoading.dismiss();
                        System.out.println("onErrorResponse()");
                        System.out.println(error.toString());
                    }
                });

                queue.add(req);


            }
        });
        DueCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    bDatePicker.setEnabled(true);
                    bTimePicker.setEnabled(true);
                    bDatePicker.setAlpha(1f);
                    bTimePicker.setAlpha(1f);
                    checkFlag =true;
                }
                else{
                    DateText.setText("Date");
                    TimeText.setText("Time");
                    bDatePicker.setEnabled(false);
                    bTimePicker.setEnabled(false);
                    bDatePicker.setAlpha(.25f);
                    bTimePicker.setAlpha(.25f);
                    checkFlag =false;
                }
            }
        });
        bDatePicker.setOnClickListener(this);
        bTimePicker.setOnClickListener(this);

    }
    @Override
    public void onClick(View v) {

        if (v == bDatePicker) {

            // Get Current Date
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);


            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            DateText.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                        }

                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
        }
        if (v == bTimePicker) {

            // Get Current Time
            final Calendar c = Calendar.getInstance();
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);

            // Launch Time Picker Dialog
            TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                    new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {

                            TimeText.setText(hourOfDay + ":" + minute);
                        }
                    }, mHour, mMinute, false);
            timePickerDialog.show();
        }
    }


}
