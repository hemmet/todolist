package com.mate.todolist;

/**
 * Created by Mert on 11.08.2017.
 */
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;


public class Notifications {
    public static void notify(Object systemService, Context context, String body){
        String title = "Not2Do";

        NotificationManager notif=(NotificationManager)systemService;
        Notification notify=new Notification.Builder
                (context).setContentTitle(title).setContentText(body).build();

        notify.flags |= Notification.FLAG_AUTO_CANCEL;
        notif.notify(0, notify);
    }
}
