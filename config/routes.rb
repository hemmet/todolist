Rails.application.routes.draw do
    
    root to: 'application#home'
    
    resources :user do
        resources :list do
            resources :item
        end
    end
    
    post '/login', to: 'user#login'
    post '/user/:id/update', to: 'user#update'
    post '/user/:user_id/list/:list_id/item/:id/update', to: 'item#update'
    post '/user/:user_id/list/:id/update', to: 'list#update'
    post '/user/:id/:fcm_token', to: 'user#fcm_token'
    get '/user/:user_id/notification', to: 'user#notification'
    
end
